// Copyright 2021 UNISOT AS

// Licensed under the Apache License, Version 2.0(the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

import fetch from "node-fetch";

const URL = "https://api.whatsonchain.com";

function getHistory(url) {
  return fetch(url).then((response) => response.json());
}

function getTransaction(url) {
  return fetch(url).then((response) => response.text());
}

export function getResolver() {
  async function resolve(did, parsed, didResolver) {

    // Basic error check
    if (did == "") {
      throw new Error(`Missing DID`);
    }
    if (!did.startsWith('did:unisot')) {
      throw new Error(`Invalid DID`);
    }
    const elements = did.split(":");
    if (!elements[2]) {
      throw new Error(`Invalid DID`);
    }

    // Parse did for API purposes
    // Parse mainnet | testnet
    const network = (elements[2].startsWith("test")) ? "test" : "main";

    // Parse URN
    const uri = (elements[2].startsWith("main") || elements[2].startsWith("test")) ? elements[3] : elements[2];

    // Parse id
    const id = uri.substring(0, 34);

    const match = (/^[a-z0-9]+$/i).test(id);
    if (!match) {
      throw new Error(`Invalid DID`);
    }

    // Fetch DID Address
    const historyUrl = URL + "/v1/bsv/" + network + "/address/" + id + '/history';
    const transactionUrl = await getHistory(historyUrl)
      .then(response => {
        // Fetch last update of the DID
        const tx_hash = response[response.length - 1].tx_hash;
        return URL + "/plugin/0/" + network + "/" + tx_hash + "/0";
      }).catch(error => {
        console.log(error.message);
      });
      
    // Fetch DID Document
    const didDoc = await getTransaction(transactionUrl.toString())
      .then(response => {
        // Strip HTML tags
        let didDocument = response.replace(/<\/?[^>]+(>|$)/g, "");
        return JSON.parse(didDocument);
      }).catch(error => {
        console.log(error.message);
      });

    return didDoc;
  }

  return { unisot: resolve };
}